
# a facultative architecture to help sharing data between component

class DataPack():
    def __init__(self):
        pass

class StatusPack(DataPack):
    def __init__(self):
        self.active = False

class PlaneData(DataPack):
    def __init__(self):
        self.yaw = 0
        self.pitch = 0
        self.roll = 0

class CommandData(DataPack):
    def __init__(self):
        self.forward = 0
        self.right = 0
        self.fire = False