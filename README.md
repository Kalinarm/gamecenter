# GameCenter

Small python scripts to link game to some hardwares on windows

## Informations

Author : Kalinarm

Author mail : kalinarm@gmail.com

## Dependency

Python 3.2

For serial communication, we use PySerial (install this if you want use with arduino)

## Documentation

You can put your wanted behaviour in a .py script in folder *mappings*. It will be automatically loaded and imported when **GameCenter.run()** is called.

Put your plugins to deblock more functionnalities in *plugins* folder.
They are imported if no errors by default in **module shared.plugins.[pluginsname]**

## Usage

Just launch GameCenter.py script.
That will call GameCenter.run(), load all the plugins and the corresponding mapping

## Participate

Merge request are welcome. Please describes properly your adding value or bugfixes.
You can share idea or wanted improve as issue : If so, add the idea label