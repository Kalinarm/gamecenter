import serial
import time

import shared

def start():
    global co
    try :
        co = serial.Serial('COM4', 9600, timeout=0)
    except:
        print("cannot open port")

def update():
    pass
    
def stop():
    print("plugin stop")

def sendData(data):
    global co
    if co is None:
        return
    data += "\r\n"
    co.write(data.encode())
