#Game center

import os
import re
import importlib
import sys
import time
import json
from pprint import pprint
import urllib.request

import shared

#configuration data class for GameCenter
class GameCenterConfig():
    def __init__(self):
         self.refreshTimeSec = 3

#Main class and hub of the app. Load plugins, profile and hold the main loop
class GameCenter():
    def __init__(self):
        #create data
        self.config = GameCenterConfig()
        self.profile = Profile()
        self.runnig = False
        #give global acces to self and data holded
        shared.center = self
        
    #load all plugins and run the main loop
    def run(self):
        print("Game center start running...")

        print("----------------------------")
        print("-Loading plugins")
        self.m_plugins = self.load_plugins()
        print("-Plugins Loaded")

        print("----------------------------")
        self.callPluginsFunc("start")
        print("-Plugins Started")

        print("----------------------------")
        print("-Starting main loop")
        self.runnig = True
        
        print("-- debug")
        #print(sys.modules["plugins"].__dict__)
        shared.plugins.PressKey(0x32)
        self.profile.save()
        self.profile.load()
        try:
            while self.runnig:
                self.callPluginsFunc("update")
                time.sleep(self.config.refreshTimeSec)
        except KeyboardInterrupt:
            self.runnig = False
        print("quitting game center")

    #stop the main loop    
    def stop(self):
        self.runnig = False

    def addGame(self):
        pass
    def removeGame(self):
        pass

    def loadProfile(self):
        pass
    def saveProfile(self):
        pass

    def load_plugins(self):
        pysearchre = re.compile('.py$', re.IGNORECASE)
        pluginfiles = filter(pysearchre.search,os.listdir(os.path.join(os.path.dirname('__file__'),'plugins')))
        form_module = lambda fp: '.' + os.path.splitext(fp)[0]
        plugs = map(form_module, pluginfiles)
        # import parent module / namespace
        importlib.import_module('plugins')
        modules = []
        for plugin in plugs:
                 if not plugin.startswith('__') and not plugin.startswith('.__'):
                     name = str(plugin)[1:]
                     print("plugin detected : " + name)
                     #try:
                     modules.append(importlib.import_module(plugin, package="plugins"))
                     #globals().update(modules[-1].__dict__)
                     sys.modules["plugins"].__dict__.update(modules[-1].__dict__)
                     #except :
                     #    print("error loading script")
                     
                     
        #globals()["gc"] = sys.modules["gc"];
        shared.plugins = sys.modules["plugins"];
        return modules
    
    def callPluginsFunc(self, funcStr):
         for x in self.m_plugins:
            print(x)
            f= None
            try :
                f = getattr(x, funcStr)
            except:
                pass
            if f is not None : f()

# a mapping is a behavior loaded from a script file
class Mapping():
    def __init__(self):
        self.name = None

    def importScript():
        pass

    def unimportScript():
        pass

# a profile is a behavior with one or more mappings inside. Can be load and save
class Profile():
    def __init__(self):
        self.name = "Noname"
        self.mapping = Mapping()
        
    def getFolder(self):
        return os.getcwd() + "/profiles/"
    
    def save(self):
        if not os.path.exists(self.getFolder()):
            os.makedirs(self.getFolder())
        path = self.getFolder() + self.name + ".prof";
        print("saving profile to " + path)
        file = open(path, "w")
        file.write("name =" + self.name)
        file.close()
        
    def load(self):
        path = self.getFolder() + self.name + ".prof";
        print("loading profile from " + path)
        file = open(path, "r")
        for line in file:
            splits = line.split("=",1)
            print("   -reading param : " + splits[0]+ " <- " + splits[1] )

    def addMapping(mappingName):
        pass

    def removeMapping(mappingName):
        pass

# main() : run the gamecenter
gameCenter = GameCenter()
try:
    gameCenter.run()
except KeyboardInterrupt:
    print("\nquit loop")


