
import shared

def start():
    print("start War thunder mapping")

def stop():
    print("stop War thunder mapping")
    
def update():
    data = shared.plugins.getURL("http://127.0.0.1:8111/indicators")
    #parse data
    if (data is not None and data["speed"]) :
        sys.stdout.write(str(data["speed"]) + "\n")
        sys.stdout.flush()

        speed = float(data["speed"])
        if (speed < 50):
            shared.plugins.sendData("r")
        else:
            shared.plugins.sendData("g")